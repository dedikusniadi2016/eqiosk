<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="box" style="margin-bottom: 15px;">
    <div class="box-header">
        <h2 class="blue"><?= 'Integration Market Place'; ?> </h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
             <div class="row">
                <div class="col-md-12">
                 <div class="col-md-3">
                    <div data-toggle="modal" data-target="#shoppeModal" style="border: 1px solid; padding: 20px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                       <img src="<?= base_url('assets/images/shoope.png'); ?>" width="100">
                         </div>
                        </div>
                        <div class="col-md-3">
                        <div data-toggle="modal" data-target="#exampleModal" style="border: 1px solid; padding: 30px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                       <img src="<?= base_url('assets/images/tokopedia.png'); ?>" width="100">
                         </div>
                        </div>
                        <div class="col-md-3">
                        <div style="border: 1px solid; padding: 35px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                        <img src="<?= base_url('assets/images/bukalapak.png'); ?>" width="100">
                        </div>
                        </div>
                        <div class="col-md-3">
                        <div style="border: 1px solid; padding: 10px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                       <img src="<?= base_url('assets/images/jdid.png'); ?>" width="100">
                         </div>
                        </div>
                    </div>
                </div> 
                <br>
                <div class="row">
                <div class="col-md-12">
                 <div class="col-md-3">
                    <div style="border: 1px solid; padding: 33px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                       <img src="<?= base_url('assets/images/blibli.png'); ?>" width="100">
                         </div>
                        </div>
                        <div class="col-md-3">
                        <div style="border: 1px solid; padding: 40px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                       <img src="<?= base_url('assets/images/zalora.png'); ?>" width="100">
                         </div>
                        </div>
                        <div class="col-md-3">
                        <div style="border: 1px solid; padding: 15px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                        <img src="<?= base_url('assets/images/lazada.png'); ?>" width="100">
                        </div>
                        </div>
                        <div class="col-md-3">
                        <div style="border: 1px solid; padding: 25px; text-align: center; box-shadow: 5px 5px 5px #888888;">
                       <img src="<?= base_url('assets/images/elevenia.png'); ?>" width="100">
                         </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Toko</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                 <img src="<?= base_url('assets/images/tokopedia.png'); ?>" width="150" height="60">
            </div>
            <div class="col-md-4">
                <img src="<?= base_url('assets/images/icon_takhingga.png'); ?>" width="90" height="40">
            </div>
            <div class="col-md-4">
                <h1> eQiosk </h1>
            </div>
          </div>
         </div>
        <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <h3> Masukan Informasi Account Anda </h3>
        <form>
          <div class="form-group">
            <label for="fsid" class="col-form-label">FS ID :</label>
            <input type="text" class="form-control" id="fsid">
          </div>
           <div class="form-group">
            <label for="client_id" class="col-form-label">Client Id :</label>
            <input type="text" class="form-control" id="client_id">
          </div>
          <div class="form-group">
            <label for="client_secret" class="col-form-label"> Client Secret :</label>
            <input type="text" class="form-control" id="client_secret">
          </div>
         <div class="form-group">
            <label for="shop_id" class="col-form-label">Shop ID :</label>
            <input type="text" class="form-control" id="shop_id">
          </div>
        </form>
            </div>
            <div class="col-md-6">
                <br>
             <div style="border: 1px solid;background: ghostwhite; padding: 5px;">
             <h3> kiat </h3>
             EQIOSK dapat membantu Anda mengelola
             semua produk, stok , pesanan dan data penjualan dari berbagai platform toko online Anda , Hanya dengan mengintegrasikan toko online Anda di Eqiosk untuk mengatur persediaan stok di toko-toko online anda secara terpadu , melihat pesanan secara real-time, menambahkan produkke semua toko anda sekaligus melihat laporan harian dari semua toko dan sebagainya.
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Mulai Sinkronasi</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="shoppeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Toko</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12">
            <div class="col-md-4">
                 <img src="<?= base_url('assets/images/shoope.png'); ?>" width="120" height="60">
            </div>
            <div class="col-md-4">
                <img src="<?= base_url('assets/images/icon_takhingga.png'); ?>" width="90" height="40">
            </div>
            <div class="col-md-4">
                <h1> eQiosk </h1>
            </div>
          </div>
         </div>
        <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
              <div> Klik "Mulai Sinkronisasi" dan Anda akan diarahkan ke lama 
               Shoppe untuk login dan otorisasi. <br><br>

               apabila Anda ingin sinkronisasi lebih dari 1 toko ,
               silahkan anda buka Seller Center Shoppe dan log out dari
               akun Anda ,  Setelah itu baru klik "Mulai Sinkronisasi" 
               di eQiosk. </div>
               <br><br>
            <button class="btn btn-primary btn-md btn-block" onclick="window.open('https://open.shopee.com/login','_blank')">Mulai Sinkronasi</button>
            </div>
            <div class="col-md-6">
                <br>
             <div style="border: 1px solid;background: ghostwhite; padding: 5px;">
             <h3> kiat </h3>
             EQIOSK dapat membantu Anda mengelola
             semua produk, stok , pesanan dan data penjualan dari berbagai platform toko online Anda , Hanya dengan mengintegrasikan toko online Anda di Eqiosk untuk mengatur persediaan stok di toko-toko online anda secara terpadu , melihat pesanan secara real-time, menambahkan produkke semua toko anda sekaligus melihat laporan harian dari semua toko dan sebagainya.
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>